## Running the analyses & compiling the manuscript PDF
To be able to run the container and the following analyses, you need to
1. Obtain the necessary input data by making a request on the
[BLSA website](https://www.blsa.nih.gov).  

2. Download this repository:
    ```
    git clone git@gitlab.com:bilgelm/tau_predictors.git
    ```

3. [Install Singularity](https://www.sylabs.io/guides/3.0/user-guide/installation.html)
and obtain the Singularity image for the analyses by running the script:
    ```
    cd tau_predictors/container
    sh pull.sh
    ```

4. Modify the paths in lines 11 and 13 of `run_container.sh` so that the
`IMAGE_DATA_DIR` and `DATA_DIR` variables are defined according to your file
organization. These should refer to the data files obtained in step 1.

5. Run the container by entering `sh run_container.sh` in the command line.
This will work as written if your working directory is `$REPO_ROOT`; otherwise
you need to prepend the path to `run_container.sh`.

6. You're now ready to carry out the analyses inside the container.
The order of analysis is as follows:
   * Generate intermediary inputs:
   ```
   Rscript /code/preprocessing/define_sample.R
   ```
   * Run nipype analysis:
   ```
   cd /code/voxelwise
   python av1451pred_nipype.py
   ```

7. Run LM and LME models in R and compile manuscript:
    ```
    cd /code/manuscript
    sh compile_manuscript.sh
    ```

    This will generate the pdf prior to the manual peak table edits.
    To see exactly what was manually edited, you can search the git repository
    history.

## Running RStudio inside container
If you run rstudio from inside the container, a bunch of hidden directories will
be created in your home directory, allowing you to retain settings/defaults/file paths
for future runs through the container.

Make sure to go to Tools > Options > Sweave and select knitr for
"Weave Rnw files using:" after you run rstudio for the first time through the container.
Alternatively, you can do the Rnw compilation using the command at (3) above.  

## Notes
There needs to be a `.Xauthority` in the home directory for X11 forwarding to work.
Copying the one generated in your `$HOME` into the
home directory that gets mounted to the singularity image seems to do the trick
(this is done in `run_container.sh`).
