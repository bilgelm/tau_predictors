# get the git repository root directory
export REPO_ROOT=`git rev-parse --show-toplevel`

# create a directory to store the singularity images
export IMAGE_DIR=$REPO_ROOT/container/build
mkdir -p $IMAGE_DIR

# cd into the directory where the singularity images will be downloaded
cd $IMAGE_DIR

# get the singularity image
singularity pull --name nipypeR.simg shub://bilgelm/tau_predictors_singularity_image:nipyper
