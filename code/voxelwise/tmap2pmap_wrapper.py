import os
import numpy as np
import nibabel as nib
from scipy import stats
from nipype.interfaces.base import TraitedSpec, File, traits, BaseInterface, BaseInterfaceInputSpec
from nipype.utils.filemanip import split_filename

class tmap2pmapInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, desc='Path to label image', mandatory=True)
    dof = traits.Int(desc='degrees of freedom', mandatory=True)

class tmap2pmapOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc='pval_map')

class tmap2pmap(BaseInterface):
    """
    Convert an image of T-values to p-values
    """

    input_spec = tmap2pmapInputSpec
    output_spec = tmap2pmapOutputSpec

    def _run_interface(self, runtime):
        in_file = self.inputs.in_file
        _, base, _ = split_filename(in_file)

        # load T-value map and convert to P-value map
        tmap = nib.load(in_file)
        tmap_data = np.array(tmap.dataobj)
        pval_data = np.ones_like(tmap_data, dtype='float64')
        eps = 1e-18 # t-values <= eps will be assigned a p-value of 1
        pval_data[np.abs(tmap_data)>eps] = stats.t.sf(np.abs(tmap_data[np.abs(tmap_data)>eps]), self.inputs.dof)*2 # two-tailed p-value

        # Save results
        pval_img = nib.Nifti1Image(pval_data, tmap.affine)
        pval_imgFile = base+'_pval.nii.gz'
        nib.save(pval_img,pval_imgFile)

        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        fname = self.inputs.in_file
        _, base, _ = split_filename(fname)

        outputs['out_file'] = os.path.abspath(base+'_pval.nii.gz')

        return outputs
