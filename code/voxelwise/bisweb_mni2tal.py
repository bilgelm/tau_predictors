from scipy import misc
import numpy as np

def convertMNIToSlice(plane, value):
    MNI = [ 90, 90, 72 ]
    #DIMENSIONS = [ 181,217,181]
    #MNIMIN = [ -90, -126,-72 ]
    #MNIMAX = [ 90, 90,108 ]
    MNIFLIP = [ True,True,False]

    a = np.round(value)
    if MNIFLIP[plane]:
        a = -value
    b = a + MNI[plane]
    return b

def mni2tal(mnicoord):
    lookup = misc.imread("/opt/bisweb/colin_talairach_lookup_xy.png")
    lookup_int16 = np.int16(lookup)

    s0 = convertMNIToSlice(0, mnicoord[0])
    s1 = convertMNIToSlice(1, mnicoord[1])
    s2 = convertMNIToSlice(2, mnicoord[2])
    voxelx = s2*181 + s0
    voxely = s1
    tal_coord = lookup_int16[voxely, voxelx,:]-128
    return tal_coord

def bisweb_mni2tal(inpoints):
    # Make inpoints into a numpy ndarray
    inpoints = np.array(inpoints)

    if not (inpoints.ndim==2 and ((inpoints.shape[0]==3) or (inpoints.shape[1]==3))):
        raise ValueError('input must be a N-by-3 or 3-by-N matrix')

    if inpoints.shape[1] == inpoints.shape[0]:
        warning('input is an ambiguous 3-by-3 matrix')
        warning('assuming coordinates are row vectors')

    transposed = False
    if inpoints.shape[1] != 3:
        # coordinates were provided in column vector format
        # transpose so that each row corresponds to a point
        transposed = True
        inpoints = np.transpose(inpoints)

    outpoints = np.apply_along_axis(mni2tal, 1, inpoints)

    if transposed:
        outpoints = np.transpose(outpoints)

    return outpoints
