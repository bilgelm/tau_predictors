import os.path as op
import numpy as np
import nibabel as nib
from nipype.interfaces.base import TraitedSpec, File, ImageFile, traits, isdefined, \
                                   BaseInterface, BaseInterfaceInputSpec, OutputMultiPath, \
                                   CommandLine, CommandLineInputSpec
from nipype.utils.filemanip import split_filename

class ExcelToTDInputSpec(CommandLineInputSpec):
    op_string = traits.String(argstr="%s", position=0,
                              mandatory=True, desc="option string, must end with a comma")
    in_file = File(exists=True, argstr="%s",
                   position=1, mandatory=True, desc='Coordinate list')

class ExcelToTDOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc='Tab-delimited file containing each coordinate and its assigned Talairach label')

class ExcelToTD(CommandLine):
    _cmd = 'java -cp /opt/talairach/talairach.jar org.talairach.ExcelToTD'
    input_spec = ExcelToTDInputSpec
    output_spec = ExcelToTDOutputSpec

    def _list_outputs(self):
        outputs = self.output_spec().get()
        outputs['out_file'] = op.abspath(self.inputs.in_file+'.td')
        return outputs
