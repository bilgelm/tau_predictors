# AV1451 PET STATISTICAL ANALYSIS
#
# Function that performs voxelwise linear regression using baseline AV1451 PET scans (with pvc)
# restricted to cognitively normal individuals
#   SUVR ~ 1 + age + pibgroup + sex + race + age*pib

# import packages
import os
os.chdir("/code/voxelwise")

import math, logging
import pandas as pd
import numpy as np
from scipy import stats
# nipype
import nipype.interfaces.io as nio
from nipype.interfaces import spm, fsl
from nipype.algorithms import misc
from nipype.pipeline.engine import Workflow, Node, JoinNode, MapNode
from nipype.interfaces.utility import IdentityInterface, Merge, Function, Split
from nipype import config, logging

from tmap2pmap_wrapper import tmap2pmap # must import wrapper within nipype function
from talairach_client_wrapper import ExcelToTD
from nipype_funcs import ROI_stats_to_spreadsheet, ConcatenateSpreadsheets, \
                         PeakTables, Nanslicer

config.enable_debug_mode()
config.update_config({'execution': {'stop_on_first_crash': True}})
logging.update_logging(config)

# -------------------------------- INPUTS --------------------------------------
# directory where processed (i.e., SUVR, normalized to MNI space) AV1451 images are
av1451_processed_dir = '/image_data/processed_suvrimages_pvc/' # must end with "/"

# mask file
maskfile = '/image_data/template/' + \
           'BLSA_SPGR+MPRAGE_AllBaselines_Age60-80_Random100_averagetemplate_short_rMNI152_reoriented_brainmask.nii'

BLSA_MUSE_MNI152 = '/image_data/template/' + \
                   'BLSA_MUSE_MNI152.nii.gz'

organization_spreadsheet = '/code/preprocessing/output/tau_baseline_sample.csv'

# output directory
output_dir = '/output'

# Full width at half max for isotropic Gaussian smoothing, in mm
smooth_fwhm = 6

# number of parallel processes
n_procs = 8

# threshold contrasts at p < 0.001 (two-tailed), k > 400 voxels for use in dual-coded figures
height_threshold_two_tailed = 0.001
height_threshold_conjmask_two_tailed = 0.05
extent_threshold = 400 # number of voxels

min_dist_bw_subpeaks = 12 # distance in mm; None means subpeaks will not be reported

# the operation string to the talairach client function we are running, ExcelToTD,
# must be in the form "3:<cubesize>," (must include comma at the end)
# where 3 means that we want to do the cube search,
# and <cubesize> can be 3, 5, 7, 9, or 11.
talairach_op_string = '3:9,'
# ------------------------------------------------------------------------------

# Access standalone SPM within container
matlab_cmd = os.environ['SPMMCRCMD']
spm.SPMCommand.set_mlab_paths(matlab_cmd=matlab_cmd, use_mcr=True)

# Read in the organization spreadsheet
data_table = pd.read_csv(organization_spreadsheet)

subset_cols = ['blsaid','AV1451blsavi','AV1451age_c','pibgroup','sex_c','raceBinary_c'] # only use relevant columns
data_table = data_table[subset_cols]

subjID = data_table['blsaid'].values.tolist()
visNo = data_table['AV1451blsavi'].values.tolist()
blsavi_integer = [ math.floor(x) for x in visNo ]
blsavi_decimal = [ str(x).split('.')[1] for x in visNo ]

idvi_list = ["%04d_%02d-%s" % idvi for idvi in zip(subjID,blsavi_integer,blsavi_decimal)]

getparticipant = Node(interface=IdentityInterface(fields=['idvi','id','vi']),
                      iterables=[('idvi', idvi_list),
                                 ('id', subjID),
                                 ('vi', visNo)],
                      synchronize=True,
                      name="getparticipant")

imgfile = os.path.join('rBLSA_{idvi}_P2_av1451_c3_mean_rbv_pvc_suvr_mni.nii.gz')
templates = {'suvr_mni': imgfile}
selectfiles = Node(interface=nio.SelectFiles(templates, base_directory=av1451_processed_dir),
                   name="selectfiles")

gunzip = Node(interface=misc.Gunzip(), name="gunzip")

smooth = Node(interface=spm.Smooth(fwhm=smooth_fwhm, use_mcr=True), name="smooth")

covariate_list = [{'vector':data_table['AV1451age_c'].tolist(),
                   'name':'age',
                   'centering':5},
                  {'vector':data_table['pibgroup'].tolist(),
                   'name':'pibgroup',
                   'centering':5},
                  {'vector':data_table['sex_c'].tolist(),
                   'name':'sex',
                   'centering':5},
                  {'vector':data_table['raceBinary_c'].tolist(),
                   'name':'race',
                   'centering':5},
                  {'vector':(data_table['AV1451age_c']*data_table['pibgroup']).tolist(),
                   'name':'age*pib',
                   'centering':5}]

mreg = JoinNode(interface=spm.MultipleRegressionDesign(covariates=covariate_list,
                                                       include_intercept=True,
                                                       global_normalization=1,
                                                       global_calc_omit=True,
                                                       threshold_mask_none=True,
                                                       use_implicit_threshold=True,
                                                       explicit_mask_file=maskfile,
                                                       no_grand_mean_scaling=True,
                                                       use_mcr=True),
                joinsource="getparticipant", joinfield="in_files",
                name="mreg")

est = Node(interface=spm.EstimateModel(estimation_method={'Classical': 1},
                                       write_residuals=False,
                                       use_mcr=True), name="est")

contrast_list = [('agePos_among_PiBneg','T',['mean','age','pibgroup','sex','race','age*pib'],[0, 1, 0, 0, 0, 0]),
                 ('agePos_among_PiBpos','T',['mean','age','pibgroup','sex','race','age*pib'],[0, 1, 0, 0, 0, 1]),
                 ('ageNeg_among_PiBneg','T',['mean','age','pibgroup','sex','race','age*pib'],[0, -1, 0, 0, 0, 0]),
                 ('ageNeg_among_PiBpos','T',['mean','age','pibgroup','sex','race','age*pib'],[0, -1, 0, 0, 0, -1]),
                 ('pibPos','T',['mean','age','pibgroup','sex','race','age*pib'],[0, 0, 1, 0, 0, 0]),
                 ('pibNeg','T',['mean','age','pibgroup','sex','race','age*pib'],[0, 0, -1, 0, 0, 0]),
                 ('sexMale','T',['mean','age','pibgroup','sex','race','age*pib'],[0, 0, 0, 1, 0, 0]),
                 ('sexFemale','T',['mean','age','pibgroup','sex','race','age*pib'],[0, 0, 0, -1, 0, 0]),
                 ('raceBlack','T',['mean','age','pibgroup','sex','race','age*pib'],[0, 0, 0, 0, 1, 0]),
                 ('raceNonblack','T',['mean','age','pibgroup','sex','race','age*pib'],[0, 0, 0, 0, -1, 0]),
                 ('agebypibPos','T',['mean','age','pibgroup','sex','race','age*pib'],[0, 0, 0, 0, 0, 1]),
                 ('agebypibNeg','T',['mean','age','pibgroup','sex','race','age*pib'],[0, 0, 0, 0, 0, -1])]

estcon = Node(interface=spm.EstimateContrast(contrasts=contrast_list,
                                             group_contrast=True,
                                             use_mcr=True),
              name="estcon")

def getdirpath(output_filelist):
    from os.path import commonpath
    return commonpath(output_filelist)

getcontrastindex = Node(interface=IdentityInterface(fields=['contrast_index']),
                        iterables=[('contrast_index',
                                    np.arange(len(contrast_list))+1)],
                        name="getcontrastindex")

# iterate over contrasts -- this way, we don't have to set each downstream node
# as a MapNode; they can simply be Nodes
templates = {'spmT_image': 'spmT_{contrast:04d}.nii',
             'con_image': 'con_{contrast:04d}.nii'}
getcontrasts = Node(interface=nio.SelectFiles(templates),
                    name="getcontrasts")


dof = len(idvi_list)-(len(covariate_list)+1)

# convert p-value threshold to t-value
t_threshold = stats.t.isf(height_threshold_two_tailed / 2, dof)

threshold = Node(interface=spm.Threshold(extent_fdr_p_threshold=1,
                                         use_topo_fdr=False,
                                         use_fwe_correction=False,
                                         height_threshold_type='stat',
                                         height_threshold=t_threshold,
                                         extent_threshold=extent_threshold,
                                         use_mcr=True),
                 name='threshold')


def nan2number(t_image_file, thresholded_t_image_file, threshold):
    from os.path import abspath
    from numpy import isnan
    from nibabel import Nifti1Image, load, save
    from nipype.utils.filemanip import split_filename

    thresholded_t_image = load(thresholded_t_image_file)
    thresholded_t_image_data = thresholded_t_image.get_data()

    t_image_data = load(t_image_file).get_data()

    # where thresholded map is nan and where the original map is < threshold,
    # keep original value
    idx = isnan(thresholded_t_image_data) & (t_image_data<threshold)
    thresholded_t_image_data[idx] = t_image_data[idx]

    # where thresholded map is nan and where the original map is >= threshold,
    # set to slightly below threshold.
    idx = isnan(thresholded_t_image_data) & (t_image_data>=threshold)
    thresholded_t_image_data[idx] = 0.99*threshold

    out_img = Nifti1Image(thresholded_t_image_data, thresholded_t_image.affine)
    _, base, _ = split_filename(thresholded_t_image_file)
    out_img_file = abspath(base+'_nanvalue.nii')
    save(out_img,out_img_file)

    return out_img_file

thresh_nan = Node(interface=Function(input_names=['t_image_file',
                                                  'thresholded_t_image_file',
                                                  'threshold'],
                                     output_names=['out_file'],
                                     function=nan2number),
                  name="thresh_nan")
thresh_nan.inputs.threshold = t_threshold

atlas_list = ['aal',
              'talairach_ba',
              'neuromorphometrics',
              'desikan_killiany',
              'harvard_oxford']

stat_plot_kws = {"title": None}

peak_tables = Node(interface=PeakTables(cluster_extent=extent_threshold,
                                        atlas=atlas_list,
                                        voxel_thresh=t_threshold,
                                        direction='pos',
                                        min_distance=min_dist_bw_subpeaks,
                                        stat_plot_kws=stat_plot_kws),
                   name="peak_tables")

def remove_nonexisting(output_filelist):
    from os.path import isfile
    return sorted([fl for fl in output_filelist if isfile(fl)])

get_list_of_peak_tables = JoinNode(interface=Function(input_names=['output_filelist'],
                                                      output_names=['existing_filelist'],
                                                      function=remove_nonexisting),
                                   joinsource="getcontrastindex", joinfield="output_filelist",
                                   name="get_list_of_peak_tables")

def prepare_peaks_for_talairach_client(peak_tables_csv):
    from os.path import abspath
    from pandas import read_csv
    from numpy import savetxt
    from nipype.utils.filemanip import split_filename
    from bisweb_mni2tal import bisweb_mni2tal

    # get the (x,y,z) coordinates from peak_tables as an N-by-3 ndarray
    mni_coords = read_csv(peak_tables_csv,
                          usecols=['peak_x','peak_y','peak_z']).to_numpy()
    tal_coords = bisweb_mni2tal(mni_coords)

    # save numpy matrix to a comma-delimited file
    _, base, _ = split_filename(peak_tables_csv)
    output_fname = abspath(base + '_tal_coords.csv')
    savetxt(output_fname, tal_coords, fmt='%.2f', delimiter=",")

    return output_fname

prepare_peaks = MapNode(interface=Function(input_names=['peak_tables_csv'],
                                           output_names=['coords'],
                                           function=prepare_peaks_for_talairach_client),
                                           iterfield=['peak_tables_csv'],
                        name="prepare_peaks")

talairach_client = MapNode(interface=ExcelToTD(op_string=talairach_op_string),
                           iterfield=['in_file'],
                           name="talairach_client")

# take absolute value of the T-value map for transparency mapping in nanslicer
tmap_abs = Node(interface=fsl.UnaryMaths(operation='abs',
                                         output_type='NIFTI'),
                name="tmap_abs")

# hone in on the middle 50th percentile with a cmap symmetric around 0
def get_lims(imgfile, maskfile):
    import nibabel as nib
    import numpy as np

    mask = nib.load(maskfile).get_data().astype(bool)

    img_dat = nib.load(imgfile).get_data()[mask]

    q1_q3 = np.percentile(img_dat, [10, 90])
    return [-np.abs(q1_q3).max(), np.abs(q1_q3).max()]

def get_predefined_lims(imgfile):
    from os.path import basename

    limsA = [-0.03, 0.03]
    limsB = [-0.4, 0.4]
    predefined_lims = {'con_0001': limsA,
                       'con_0002': limsA,
                       'con_0003': limsA,
                       'con_0004': limsA,
                       'con_0005': limsB,
                       'con_0006': limsB,
                       'con_0007': limsB,
                       'con_0008': limsB,
                       'con_0009': limsB,
                       'con_0010': limsB,
                       'con_0011': limsA,
                       'con_0012': limsA}

    return predefined_lims[basename(imgfile)[:8]]

def get_best_coord(peaks_csv):
    # get the coords of peak in the largest cluster with the largest T value

    from os.path import isfile, basename

    if isfile(peaks_csv):
        if basename(peaks_csv)=='spmT_0002_peaks.csv':
            coords = [44, 30, 0]
        else:
            from pandas import read_csv
            peaks = read_csv(peaks_csv,
                              usecols=['peak_x','peak_y','peak_z',
                                       'peak_value','volume_mm'])
            # prioritize central slices
            peaks['priority'] = (abs(peaks[['peak_x','peak_y','peak_z']])<40).all(axis=1)
            # sort by priority, cluster size, t value
            peaks.sort_values(by=['priority','volume_mm','peak_value'],
                              ascending=False, inplace=True)
            coords = peaks[['peak_x','peak_y','peak_z']].values[0].tolist()
    else:
        coords = [-11, 29, 3]

    return coords

nanslicer = Node(interface=Nanslicer(base_image=BLSA_MUSE_MNI152,
                                     overlay_map='jet',
                                     overlay_label="'Regression coefficient'",
                                     overlay_alpha_lim=[0, 5],
                                     overlay_alpha_label="'|T|'",
                                     contour=[t_threshold],
                                     slice_lims=[0.3, 0.7],
                                     bar_pos='right',
                                     three_axis=False),
                 name="nanslicer")

nanslicer_3ax = nanslicer.clone(name="nanslicer_3ax")
nanslicer_3ax.inputs.bar_pos = 'bottom'
nanslicer_3ax.inputs.three_axis = True
nanslicer_3ax.inputs.dpi = 300


# --------- CONJUNCTION MASK
conjmask_contrasts = ['agePos_among_PiBpos','pibPos','agebypibPos']
conjmask_contrastIDname = [(i+1, contrast[0]) for i, contrast in enumerate(contrast_list) if contrast[0] in conjmask_contrasts]

# iterate over contrasts and associate each contrast with a name
templates = {'spmT_image': 'spmT_{conjmask_contrast}.nii'}
selectcontrasts = Node(interface=nio.SelectFiles(templates), #base_directory=os.path.join(voxelwise_wf.base_dir,'voxelwise_wf','tmap2pmap','mapflow')),
                       iterables=[('conjmask_contrast', ['{:04d}'.format(contrast[0]) for contrast in conjmask_contrastIDname])],
                       name="selectcontrasts")

# convert T-value map to p-values for use in conjunction map
tmap_thresh = Node(interface=fsl.Threshold(thresh=0, direction='below'),
                   name = 'tmap_thresh')
tmap2pmap = Node(interface=tmap2pmap(dof = dof),
                 name="tmap2pmap")

thresh_and_binarize_pval = Node(interface=fsl.ImageMaths(op_string='-mul -1 -add 1 -thr 0.95 -bin',
                                                         suffix='_thr_bin'),
                                name="thresh_and_binarize_pval")
merge = JoinNode(interface=fsl.Merge(dimension='t'),
                 joinsource="selectcontrasts", joinfield="in_files",
                 name="merge")

multiply_masks = Node(interface=fsl.ImageMaths(op_string='-Tmin', suffix='_conjmask'),
                      name="multiply_masks")

glass_plot_kws = {"colorbar": False,
                  "title": 'Conjunction Mask',
                  "threshold": None,
                  "annotate": True,
                  "black_bg": False,
                  "cmap": None,
                  "alpha": 0.7,
                  "vmin": None,
                  "vmax": None,
                  "resampling_interpolation": 'nearest'}

# conjmask table and snapshot
conjmask_table = Node(interface=PeakTables(cluster_extent=extent_threshold,
                                           atlas=atlas_list,
                                           voxel_thresh=0.5,
                                           direction='pos',
                                           glass_plot_kws=glass_plot_kws),
                      name="conjmask_table")

# compute mean SUVR within conjunction mask
ROImeans_suvr = Node(interface=ROI_stats_to_spreadsheet(ROI_list=[1],
                                                        ROI_names=['conj_mask'],
                                                        stat='mean'),
                     name="ROImeans_suvr")

SUVR_csv = JoinNode(interface=ConcatenateSpreadsheets(outputname='ROI_SUVR'),
                    joinsource='getparticipant', joinfield=['sheetlist'],
                    unique=True, name='SUVR_csv')

interaction_contrastID = [contrast[0] for contrast in contrast_list].index('agebypibPos')+1
selectinteraction = Node(interface=nio.SelectFiles({'spmT_image': 'spmT_{interaction_contrast}.nii'}),
                         iterables=[('interaction_contrast', ['{:04d}'.format(interaction_contrastID)])],
                         name="selectinteraction")
mask_Tval = Node(interface=fsl.ImageMaths(op_string="-mul"),
                 name='mask_Tval')

# convert p-value threshold to t-value
t_threshold_conjmask = stats.t.isf(height_threshold_conjmask_two_tailed / 2, dof)
# conjmask table and snapshot
conjmask_peak_table = Node(interface=PeakTables(cluster_extent=extent_threshold,
                                                atlas=atlas_list,
                                                voxel_thresh=t_threshold_conjmask,
                                                direction='pos',
                                                min_distance=min_dist_bw_subpeaks,
                                                glass_plot_kws=glass_plot_kws,
                                                stat_plot_kws = {"title": None}),
                           name="conjmask_peak_table")

prepare_conjmask_peaks = Node(interface=Function(input_names=['peak_tables_csv'],
                                           output_names=['coords'],
                                           function=prepare_peaks_for_talairach_client),
                        name="prepare_conjmask_peaks")

# the operation string to the talairach client function we are running, ExcelToTD,
# must be in the form "3:<cubesize>," (must include comma at the end)
# where 3 means that we want to do the cube search,
# and <cubesize> can be 3, 5, 7, 9, or 11.
talairach_client_conjmask = Node(interface=ExcelToTD(op_string=talairach_op_string),
                                 name="talairach_client_conjmask")

def get_tvals_at_MNIcoords(Tval_image_fnames, peak_table_csv):
    from atlasreader.atlasreader import coord_xyz_to_ijk
    from nilearn._utils import check_niimg
    from pandas import read_csv
    from nipype.utils.filemanip import split_filename

    peak_table = read_csv(peak_table_csv)
    mni_coords = peak_table[['peak_x','peak_y','peak_z']].to_numpy()

    for Tval_image_fname in Tval_image_fnames:
        Tval_image = check_niimg(Tval_image_fname)
        coords = coord_xyz_to_ijk(Tval_image.affine, mni_coords)
        Tvals_at_coords = Tval_image.get_data()[tuple(map(tuple, coords.T))]

        _, base, _ = split_filename(Tval_image_fname)
        peak_table[base] = Tvals_at_coords

    peak_table.to_csv(peak_table_csv, index=False)

    return peak_table_csv


tvals_at_MNIcoords = JoinNode(interface=Function(input_names=['Tval_image_fnames','peak_table_csv'],
                                                 output_names=['peak_table_csv'],
                                                 function=get_tvals_at_MNIcoords),
                              joinsource="selectcontrasts", joinfield="Tval_image_fnames",
                              name="tvals_at_MNIcoords")

# set up output directory
datasink = Node(interface=nio.DataSink(base_directory = output_dir,
                                       container = 'voxelwise_output',
                                       substitutions = [('peak_tables/_contrast_index_{:d}/'.format(i+1), 'peak_tables/' + contrast[0] + '/')
                                                        for i, contrast in enumerate(contrast_list)] +
                                                       [('conjmask_peak_table/_interaction_contrast_{:04d}/'.format(i+1), 'conjmask_peak_table/' + contrast[0] + '/')
                                                        for i, contrast in enumerate(contrast_list)] +
                                                       [('talairach_client_conjmask/_interaction_contrast_{:04d}/'.format(i+1), 'talairach_client_conjmask/')
                                                        for i, _ in enumerate(contrast_list)] +
                                                       [('_contrast_index_{:d}/con_{:04d}_spmT_{:04d}_abs.png'.format(i+1,i+1,i+1), contrast[0] + '.png')
                                                        for i, contrast in enumerate(contrast_list)] +
                                                       [('_maths','')],
                                       regexp_substitutions = [(r'spmT_.*_conjmask',r'conjmask'),
                                                               (r'_prepare_peaks\d/',r'/')]),
                name="datasink")

voxelwise_wf = Workflow(name="voxelwise_wf")
voxelwise_wf.base_dir = output_dir
voxelwise_wf.config = {"execution": {"crashdump_dir": os.path.join(output_dir,'voxelwise_crashdumps')}}
voxelwise_wf.connect([(getparticipant, selectfiles, [('idvi','idvi')]),
                      (selectfiles, gunzip, [('suvr_mni','in_file')]),
                      (gunzip, smooth, [('out_file','in_files')]),
                      (smooth, mreg, [('smoothed_files','in_files')]),
                      (mreg, est, [('spm_mat_file','spm_mat_file')]),
                      (est, estcon, [('spm_mat_file','spm_mat_file'),
                                     ('residual_image','residual_image'),
                                     ('beta_images','beta_images')]),

                      # iterator over contrasts
                      (getcontrastindex, getcontrasts, [('contrast_index','contrast')]),
                      (estcon, getcontrasts, [(('spmT_images', getdirpath),'base_directory')]),

                      (estcon, threshold, [('spm_mat_file','spm_mat_file')]),
                      (getcontrastindex, threshold, [('contrast_index','contrast_index')]),
                      (getcontrasts, tmap_abs, [('spmT_image', 'in_file')]),
                      (tmap_abs, threshold, [('out_file','stat_image')]),
                      (threshold, thresh_nan, [('thresholded_map','thresholded_t_image_file')]),
                      (tmap_abs, thresh_nan, [('out_file','t_image_file')]),

                      # cluster and (sub)peak tables
                      (getcontrasts, peak_tables, [('spmT_image','filename')]),

                      # feed the (sub)peak coordinates into the Talairach client to get Brodmann areas
                      (peak_tables, get_list_of_peak_tables, [('peaks_csv','output_filelist')]),
                      (get_list_of_peak_tables, prepare_peaks, [('existing_filelist','peak_tables_csv')]),
                      (prepare_peaks, talairach_client, [('coords','in_file')]),

                      #(getcontrastindex, getcomplementcontrastindex, [('contrast_index','contrast_index')]),

                      # generate dualcode snapshots
                      # - axial snapshots
                      (getcontrasts, nanslicer, [('con_image','overlay'),
                                                 (('con_image', get_lims, maskfile), 'overlay_lim')]),
                      (tmap_abs, nanslicer, [('out_file','overlay_alpha')]),
                      (thresh_nan, nanslicer, [('out_file','overlay_contour')]),
                      # - triplanar snapshots
                      (getcontrasts, nanslicer_3ax, [('con_image','overlay'),
                                                     (('con_image', get_predefined_lims), 'overlay_lim')]),
                      (tmap_abs, nanslicer_3ax, [('out_file','overlay_alpha')]),
                      (thresh_nan, nanslicer_3ax, [('out_file','overlay_contour')]),
                      (peak_tables, nanslicer_3ax, [(('peaks_csv', get_best_coord),'slice_pos')]),

                      # conjunction mask
                      (estcon, selectcontrasts, [(('spmT_images', getdirpath),'base_directory')]),

                      (selectcontrasts, tmap_thresh, [('spmT_image','in_file')]),
                      (tmap_thresh, tmap2pmap, [('out_file','in_file')]),

                      (tmap2pmap, thresh_and_binarize_pval, [('out_file','in_file')]),
                      (thresh_and_binarize_pval, merge, [('out_file', 'in_files')]),
                      (merge, multiply_masks, [('merged_file', 'in_file')]),
                      (multiply_masks, conjmask_table, [('out_file','filename')]),

                      (estcon, selectinteraction, [(('spmT_images', getdirpath),'base_directory')]),
                      (selectinteraction, mask_Tval, [('spmT_image','in_file')]),
                      (multiply_masks, mask_Tval, [('out_file','in_file2')]),
                      (mask_Tval, conjmask_peak_table, [('out_file','filename')]),
                      (conjmask_peak_table, prepare_conjmask_peaks, [('peaks_csv','peak_tables_csv')]),
                      (prepare_conjmask_peaks, talairach_client_conjmask, [('coords','in_file')]),

                      (selectcontrasts, tvals_at_MNIcoords, [('spmT_image','Tval_image_fnames')]),
                      (conjmask_peak_table, tvals_at_MNIcoords, [('peaks_csv','peak_table_csv')]),

                      # compute mean SUVR within conjunction mask
                      (getparticipant, ROImeans_suvr, [('id','id')]),
                      (getparticipant, ROImeans_suvr, [('vi','vi')]),
                      (selectfiles, ROImeans_suvr, [('suvr_mni','imgFile')]),
                      (multiply_masks, ROImeans_suvr, [('out_file','labelImgFile')]),

                      (ROImeans_suvr, SUVR_csv, [('csvFile','sheetlist')]),

                      # organize outputs
                      (estcon, datasink, [('spmT_images','SPM'),
                                          ('con_images','SPM.@con'),
                                          ('spm_mat_file','SPM.@mat')]), # paths in SPM.mat must be updated after this copying
                      (est, datasink, [('mask_image','SPM.@mask'),
                                       ('beta_images','SPM.@beta'),
                                       ('residual_image','SPM.@resid'),
                                       ('RPVimage','SPM.@rpv')]),

                      (multiply_masks, datasink, [('out_file','conjunction_mask')]),
                      (conjmask_table, datasink, [('snapshot','conjunction_mask.@snapshot'),
                                                  ('peaks_csv','conjunction_mask.@peaks'),
                                                  ('clusters_csv','conjunction_mask.@clusters')]),
                      (SUVR_csv, datasink, [('concatenatedlist','conjunction_mask.@SUVR_csv')]),

                      (peak_tables, datasink, [('peaks_csv','peak_tables'),
                                               ('clusters_csv','peak_tables.@clusters'),
                                               ('snapshot','peak_tables.@snap'),
                                               ('cluster_snapshots','peak_tables.@clust_snap')]),
                      (conjmask_peak_table, datasink, [('peaks_csv','conjmask_peak_table'),
                                                       ('clusters_csv','conjmask_peak_table.@clusters'),
                                                       ('snapshot','conjmask_peak_table.@snap'),
                                                       ('cluster_snapshots','conjmask_peak_table.@clust_snap')]),

                      (nanslicer, datasink, [('snapshot','regression_snapshots.axial')]),
                      (nanslicer_3ax, datasink, [('snapshot','regression_snapshots.three_axis')]),

                      (talairach_client, datasink, [('out_file','talairach_client')]),
                      (talairach_client_conjmask, datasink, [('out_file','talairach_client_conjmask')]),
                     ])

voxelwise_wf.write_graph('voxelwise_wf.dot', graph2use='colored', simple_form=True)

result = voxelwise_wf.run('MultiProc', plugin_args={'n_procs': n_procs})
