import os.path as op
import numpy as np
import nibabel as nib
from nipype.interfaces.base import TraitedSpec, File, ImageFile, traits, isdefined, \
                                   BaseInterface, BaseInterfaceInputSpec, OutputMultiPath, \
                                   CommandLine, CommandLineInputSpec
from nipype.utils.filemanip import split_filename

class ROI_stats_to_spreadsheetInputSpec(BaseInterfaceInputSpec):
    id = traits.String(desc='Participant ID', mandatory=True)
    vi = traits.String(desc='Visit', mandatory=True)
    imgFile = ImageFile(exists=True, desc='Image list', mandatory=True)
    labelImgFile = ImageFile(exists=True, desc='Label image list', mandatory=True)
    ROI_list = traits.List(traits.Int(), minlen=1,
                           desc='list of ROI indices for which stats will be computed (should match the label indices in the label image)',
                           mandatory=True)
    ROI_names = traits.List(traits.String(), minlen=1,
                            desc='list of equal size to ROI_list that lists the corresponding ROI names',
                            mandatory=True)
    additionalROIs = traits.List(traits.List(traits.Int()), desc='list of lists of integers')
    additionalROI_names = traits.List(traits.String(),
                                      desc='names corresponding to additional ROIs')
    stat = traits.Enum('mean','Q1','median','Q3','min','max',
                       desc='one of: mean, Q1, median, Q3, min, max',
                       mandatory=True)

class ROI_stats_to_spreadsheetOutputSpec(TraitedSpec):
    csvFile = File(exists=True, desc='csv file')

class ROI_stats_to_spreadsheet(BaseInterface):
    """
    Compute ROI statistics and write to spreadsheet

    """

    input_spec = ROI_stats_to_spreadsheetInputSpec
    output_spec = ROI_stats_to_spreadsheetOutputSpec

    def _run_interface(self, runtime):
        import csv

        id = self.inputs.id
        vi = self.inputs.vi
        imgFile = self.inputs.imgFile
        labelImgFile = self.inputs.labelImgFile
        ROI_list = self.inputs.ROI_list
        ROI_names = self.inputs.ROI_names
        additionalROIs = self.inputs.additionalROIs
        additionalROI_names = self.inputs.additionalROI_names
        stat = self.inputs.stat

        _, base, _ = split_filename(self.inputs.imgFile)
        csvfile = op.abspath(base+'_ROI_stats_'+stat+'.csv')

        assert(len(ROI_list)==len(ROI_names))
        assert(len(additionalROIs)==len(additionalROI_names))

        # csv file
        wf = open(csvfile, mode='w')
        writer = csv.writer(wf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        row_content = []
        row = 0
        col = 0
        row_content.append('id')
        col += 1
        row_content.append('vi')
        col += 1
        row_content.append('image path')
        col += 1
        row_content.append('label image path')
        for ROI in ROI_list:
            col += 1
            row_content.append(str(ROI))
        if isdefined(additionalROIs):
            for ROI in additionalROIs:
                col += 1
                row_content.append(str(ROI))
        writer.writerow(row_content)

        row_content = []
        row = 1
        col = 0
        row_content.append('id')
        col += 1
        row_content.append('vi')
        col += 1
        row_content.append('image path')
        col += 1
        row_content.append('label image path')
        for ROI_name in ROI_names:
            col += 1
            row_content.append(ROI_name)
        if isdefined(additionalROI_names):
            for ROI_name in additionalROI_names:
                col += 1
                row_content.append(ROI_name)
        writer.writerow(row_content)

        row_content = []
        row = 2

        image = nib.load(imgFile)
        image_dat = image.get_data()

        labelimage = nib.load(labelImgFile)
        labelimage_dat = labelimage.get_data()

        #ROI_list = np.unique(labelimage_dat)
        bn = op.basename(imgFile)
        col = 0
        row_content.append(id)
        col += 1
        row_content.append(vi)
        col += 1
        row_content.append(imgFile)
        col += 1
        row_content.append(labelImgFile)

        for ROI in ROI_list:
            ROI_mask = labelimage_dat==ROI
            if ROI_mask.sum()>0:
                if stat=="mean":
                    ROI_stat = image_dat[ROI_mask].mean()
                elif stat=="Q1":
                    ROI_stat = np.percentile(image_dat[ROI_mask], 25)
                elif stat=="median":
                    ROI_stat = np.percentile(image_dat[ROI_mask], 50)
                elif stat=="Q3":
                    ROI_stat = np.percentile(image_dat[ROI_mask], 75)
                elif stat=="min":
                    ROI_stat = np.min(image_dat[ROI_mask])
                elif stat=="max":
                    ROI_stat = np.max(image_dat[ROI_mask])
                else:
                    ROI_stat=''
                if np.isnan(ROI_stat):
                    ROI_stat = ''
            else:
                ROI_stat = ''
            col += 1
            row_content.append(ROI_stat)

        if isdefined(additionalROIs):
            for compositeROI in additionalROIs:
                ROI_mask = labelimage_dat==compositeROI[0]
                if len(compositeROI)>1:
                    for compositeROImember in compositeROI[1:]:
                        ROI_mask = ROI_mask | (labelimage_dat==compositeROImember)
                if ROI_mask.sum()>0:
                    if stat=="mean":
                        ROI_stat = image_dat[ROI_mask].mean()
                    elif stat=="Q1":
                        ROI_stat = np.percentile(image_dat[ROI_mask], 25)
                    elif stat=="median":
                        ROI_stat = np.percentile(image_dat[ROI_mask], 50)
                    elif stat=="Q3":
                        ROI_stat = np.percentile(image_dat[ROI_mask], 75)
                    elif stat=="min":
                        ROI_stat = np.min(image_dat[ROI_mask])
                    elif stat=="max":
                        ROI_stat = np.max(image_dat[ROI_mask])
                    else:
                        ROI_stat=''
                    if np.isnan(ROI_stat):
                        ROI_stat = ''
                else:
                    ROI_stat = ''
                col += 1
                row_content.append(ROI_stat)

        writer.writerow(row_content)
        wf.close()

        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()

        _, base, _ = split_filename(self.inputs.imgFile)

        outputs['csvFile'] = op.abspath(base+'_ROI_stats_'+self.inputs.stat+'.csv')

        return outputs


class ConcatenateSpreadsheetsInputSpec(BaseInterfaceInputSpec):
    sheetlist = traits.List(File, minlen=2, mandatory=True,
                            desc='list of spreadsheets to concatenate')
    outputname = traits.String(mandatory=True, desc='name of output spreadsheet without extension')

class ConcatenateSpreadsheetsOutputSpec(TraitedSpec):
    concatenatedlist = File(exists=True, desc='concatenated spreadsheet')

class ConcatenateSpreadsheets(BaseInterface):
    input_spec = ConcatenateSpreadsheetsInputSpec
    output_spec = ConcatenateSpreadsheetsOutputSpec

    def _run_interface(self, runtime):
        import pandas as pd

        sheetlist = self.inputs.sheetlist
        outputname = self.inputs.outputname

        # read in first spreadsheet
        firstsheet = pd.read_csv(sheetlist[0])

        for sheetname in sheetlist[1:]:
            print("************")
            print(sheetname)
            sheet = pd.read_csv(sheetname)
            # check that column names are equal
            if not np.array_equal(firstsheet.columns.values, sheet.columns.values):
                raise ValueError('Column headers differ')

            # check that first rows are equal
            if not firstsheet.loc[0,:].equals(sheet.loc[0,:]):
                raise ValueError('First rows differ')

            # append to firstsheet
            firstsheet = firstsheet.append(sheet.loc[1,:], ignore_index=True)

        # save spreadsheet
        firstsheet.to_csv(op.abspath(self.inputs.outputname+'.csv'), index=False)

        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs['concatenatedlist'] = op.abspath(self.inputs.outputname+'.csv')

        return outputs

class PeakTablesInputSpec(BaseInterfaceInputSpec):
    filename = ImageFile(exists=True, desc='A 3D statistical image', mandatory=True)
    cluster_extent = traits.Int(desc='Minimum number of contiguous voxels required to consider a cluster in `filename`', mandatory=True)
    atlas = traits.List(traits.String(), minlen=1, mandatory=False,
                        desc='Name of atlas(es) to consider for cluster analysis.')
    voxel_thresh = traits.Float(desc='Threshold to apply to `stat_img`. Use `direction` to specify the \
        directionality of the threshold. If a negative number is provided a \
        percentile threshold is used instead, where the percentile is \
        determined by the equation `100 - voxel_thresh`', mandatory=False)
    direction = traits.Enum('both','pos','neg',
                       desc="Specifies the direction in which `voxel_thresh` should be applied. Possible values are 'both', 'pos' or 'neg'. Default: 'both'",
                       mandatory=False)
    prob_thresh = traits.Int(desc='Probability (percentage) threshold to apply to `atlas`, if it is probabilistic.',
                             mandatory=False)
    min_distance = traits.Float(desc='Specifies the minimum distance (in mm) required between sub-peaks in a \
        cluster. If None, sub-peaks will not be examined and only the primary \
        cluster peak will be reported.', mandatory=False)
    glass_plot_kws = traits.Dict(desc='Additional keyword arguments to pass to `nilearn.plotting.plot_glass_brain.`',
                                 mandatory=False)
    stat_plot_kws = traits.Dict(desc='Additional keyword arguments to pass to `nilearn.plotting.plot_stat_map.`',
                                mandatory=False)

class PeakTablesOutputSpec(TraitedSpec):
    peaks_csv = File(exists=False, desc='peaks spreadsheet')
    clusters_csv = File(exists=False, desc='clusters spreadsheet')
    snapshot = File(exists=True, desc='snapshot')
    cluster_snapshots = OutputMultiPath(File(exists=True), desc='cluster snapshots')

class PeakTables(BaseInterface):
    # wrapper around atlasreader.create_output

    input_spec = PeakTablesInputSpec
    output_spec = PeakTablesOutputSpec

    def _run_interface(self, runtime):
        import atlasreader

        outdir = op.abspath('')

        atlas = self.inputs.atlas
        if not isdefined(atlas):
            atlas = 'default'

        voxel_thresh = self.inputs.voxel_thresh
        if not isdefined(voxel_thresh):
            voxel_thresh = 1.96

        direction = self.inputs.direction
        if not isdefined(direction):
            direction = 'both'

        prob_thresh = self.inputs.prob_thresh
        if not isdefined(prob_thresh):
            prob_thresh = 5

        min_distance = self.inputs.min_distance
        if not isdefined(min_distance):
            min_distance = None

        glass_plot_kws = self.inputs.glass_plot_kws
        if not isdefined(glass_plot_kws):
            glass_plot_kws = None

        stat_plot_kws = self.inputs.stat_plot_kws
        if not isdefined(stat_plot_kws):
            stat_plot_kws = None

        atlasreader.create_output(filename=self.inputs.filename,
                                  cluster_extent=self.inputs.cluster_extent,
                                  atlas=atlas,
                                  voxel_thresh=voxel_thresh,
                                  direction=direction,
                                  prob_thresh=prob_thresh,
                                  min_distance=min_distance,
                                  outdir=outdir,
                                  glass_plot_kws=glass_plot_kws,
                                  stat_plot_kws=stat_plot_kws)
        return runtime

    def _list_outputs(self):
        from glob import glob
        outputs = self._outputs().get()

        _, base, _ = split_filename(self.inputs.filename)

        outputs['peaks_csv'] = op.abspath(base+'_peaks.csv')
        outputs['clusters_csv'] = op.abspath(base+'_clusters.csv')
        outputs['cluster_snapshots'] = glob(op.abspath(base+'_cluster*.png'))
        outputs['snapshot'] = op.abspath(base+'.png')

        return outputs

class NanslicerInputSpec(CommandLineInputSpec):
    base_image = ImageFile(exists=True, desc="Base (structural image)",
                           argstr="%s", position=1, mandatory=True)
    out_filename = File(genfile=True, argstr="%s", position=2,
                        desc="Output snapshot image name")
    mask = ImageFile(exists=False, argstr='--mask %s',
                     desc="Mask image", mandatory=False)
    overlay = ImageFile(exists=False, argstr='--overlay %s',
                        desc="Add color overlay", mandatory=False)
    overlay_map = traits.String(argstr='--overlay_map %s',
                                desc="Overlay colormap, default = RdYlBu_r",
                                mandatory=False)
    overlay_lim = traits.List(traits.Float,
                              argstr='--overlay_lim %s', sep=' ',
                              minlen=2, maxlen=2,
                              desc="Overlay window", mandatory=False)
    overlay_mask = ImageFile(exists=False, argstr='--overlay_mask %s',
                             desc="Mask color image", mandatory=False)
    overlay_mask_thresh = traits.Float(argstr='--overlay_mask_thresh %s',
                                       desc="Overlay mask threshold",
                                       mandatory=False)
    overlay_scale = traits.Float(argstr='--overlay_scale %s',
                desc="Scaling for overlay image before mapping, default=1.0",
                mandatory=False)
    overlay_label = traits.String(argstr='--overlay_label %s',
                                  desc="Label for overlay color axis",
                                  mandatory=False)
    overlay_alpha = ImageFile(exists=False, argstr='--overlay_alpha %s',
                              desc='Image for transparency-coding of overlay',
                              mandatory=False)
    overlay_alpha_lim = traits.List(traits.Float,
                                    argstr='--overlay_alpha_lim %s', sep=' ',
                                    minlen=2, maxlen=2,
                                    desc='Overlay Alpha/transparency window',
                                    mandatory=False)
    overlay_alpha_scale = traits.Float(argstr='--overlay_alpha_scale %s',
                                       desc='Scaling factor for the alpha image',
                                       mandatory=False)
    overlay_alpha_label = traits.String(argstr='--overlay_alpha_label %s',
                                        desc="Label for overlay alpha/transparency axis",
                                        mandatory=False)
    overlay_contour = ImageFile(exists=False, argstr='--overlay_contour %s',
                                desc='Image for contour drawing',
                                mandatory=False)
    contour = traits.List(traits.Float, argstr='--contour %s', sep=' ', minlen=1,
                          desc='Add alpha image contours (can be multiple)',
                          mandatory=False)
    contour_color = traits.List(traits.String, argstr='--contour_color %s',
                                sep=' ', minlen=1,
                                desc="Choose contour colors", mandatory=False)
    contour_style = traits.List(traits.String, argstr='--contour_style %s',
                                sep=' ', minlen=1,
                                desc="Choose contour styles", mandatory=False)
    three_axis = traits.Bool(argstr='--three_axis',
                             desc="Make a 3 axis (x,y,z) plot", mandatory=False)
    slice_pos = traits.List(traits.String, argstr='--slice_pos %s', sep=' ',
                             minlen=3, maxlen=3,
                             desc='(x,y,z) coordinates to slice at (for the 3 axis plot)',
                             mandatory=False)
    slice_lims = traits.List(traits.String, argstr='--slice_lims %s', sep=' ',
                             minlen=2, maxlen=2,
                             desc='Slice between these limits along the axis, default=0.1 0.9',
                             mandatory=False)
    bar_pos = traits.Enum('bottom','right',
                          argstr='--bar_pos %s',
                          desc="Position of color-bar (bottom / right)",
                          mandatory=False)
    figsize = traits.List(traits.Float, argstr='--figsize %s', sep=' ',
                          minlen=2, maxlen=2,
                          desc="Figure size (width, height) in inches",
                          mandatory=False)
    dpi = traits.Int(argstr='--dpi %d', desc="DPI for output figure",
                     mandatory=False)

class NanslicerOutputSpec(TraitedSpec):
    snapshot = File(exists=True, desc='snapshot')

class Nanslicer(CommandLine):
    _cmd = 'nanslicer'
    input_spec = NanslicerInputSpec
    output_spec = NanslicerOutputSpec

    def _list_outputs(self):
        outputs = self.output_spec().get()
        outputs['snapshot'] = self.inputs.out_filename
        if not isdefined(outputs['snapshot']):
            outputs['snapshot'] = op.abspath(self._gen_outfilename())
        else:
            outputs['snapshot'] = op.abspath(outputs['snapshot'])
        return outputs

    def _gen_filename(self, name):
        if name == 'out_filename':
            return self._gen_outfilename()
        else:
            return None

    def _gen_outfilename(self):
        if isdefined(self.inputs.overlay):
            _, outname, _ = split_filename(self.inputs.overlay)

            if isdefined(self.inputs.overlay_alpha):
                _, name, _ = split_filename(self.inputs.overlay_alpha)
                outname = outname + '_' + name
        else:
            _, outname, _ = split_filename(self.inputs.base_image)

        return outname + '.png'
