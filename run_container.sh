set -eu

# get the git repository root directory
REPO_ROOT=`git rev-parse --show-toplevel`

# create a directory (if doesn't exist) that will be mounted as $HOME inside the container
CONTAINER_HOME=$REPO_ROOT/container/home
mkdir -p $CONTAINER_HOME

# location of AV1451 SUVR images that will be used as input for statistical analyses
IMAGE_DATA_DIR=/cog/murat/tau_predictors/image_data
# location of input spreadsheets
DATA_DIR=/cog/murat/tau_predictors/data/May2018
# location of code to run inside the container
CODE_DIR=$REPO_ROOT/code

# create output directory (if doesn't exist)
OUTPUT_DIR=$REPO_ROOT/output
mkdir -p $OUTPUT_DIR

# copy the .Xauthority file needed for X11 forwarding from the user's HOME
# to the directory that will serve as HOME inside the container
cp ~/.Xauthority $CONTAINER_HOME/.Xauthority

# run singularity using minimal system directory sharing between host and container
# redirect HOME because the container will generate a bunch of hidden files there
# (these will help pick up from where you left off when you run the container again)
# mount data and code directories
singularity run -c -H $CONTAINER_HOME \
                   -B $IMAGE_DATA_DIR:/image_data:ro \
                   -B $DATA_DIR:/data:ro \
                   -B $CODE_DIR:/code \
                   -B $OUTPUT_DIR:/output \
                   $REPO_ROOT/container/build/nipypeR.simg

# remove the copy of .Xauthority for good measure
rm $CONTAINER_HOME/.Xauthority
